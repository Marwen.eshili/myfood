import Logo from "./components/Logo";
import Header from "./Layout/header";
import Dishes from "./views/Dishes";
import Home from "./views/Home";
import Menu from "./views/Menu";
import About from "./views/About";
import Reviews from "./views/Reviews";
import Order from "./views/Order";
import Contact from "./views/Contact";
import Footer from "./Layout/Footer";

function App() {
  return (
    <>
      <Header />
      <Home />
      <Dishes />
      <About />
      <Menu />
      <Reviews />
      <Order />
      <Contact />
      <Footer />
    </>
  );
}

export default App;
