import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./main.scss";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./Store";
import { ModalsProvider } from "./components";

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
      <ModalsProvider />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
