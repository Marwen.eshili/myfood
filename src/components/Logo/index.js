import { FaUtensils } from "react-icons/fa";

const Logo = () => {
  return (
    <div className="logo">
      <i>
        <FaUtensils />
      </i>
      <span className="text-secondary">Food</span>
    </div>
  );
};

export default Logo;
