import React from "react";
import Button from "../../components/Button";
import { Card } from "@mui/material";
import dish from "../../assets/images/pic-4.png";
import HalfRating from "../Rating";
import BtnIcon from "../Icon-Button";
import { FaHeart, FaEye } from "react-icons/fa";

function CardMenu(props) {
  return (
    <Card style={{padding:"15px"}} className="card_menu">
      <div className="section_card_menu">
      <img src={props.image}></img>
      <HalfRating />
      <h3>Delicious Food</h3>
      <p>Lorem Ipsum Dolor Sit, Amet Consectetur Adipisicing Elit. Excepturi, Accusantium.</p>
     <div className="card_footer">
     <Button text="Add To Cart" /><h3>$15.99</h3>
     </div>
      </div>
    </Card>
  );
}

export default CardMenu;
