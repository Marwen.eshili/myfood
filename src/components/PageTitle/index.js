import React from "react";

function Title(props) {
  return (
    <div className="title_page">
      <h3 class="sub-heading">{props.subTitle}</h3>
      <h1 class="heading">{props.title}</h1>
    </div>
  );
}

export default Title;
