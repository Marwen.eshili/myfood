import React from "react";
import { Box ,Card} from "@material-ui/core";

function BtnBox(props) {
  return (
      <Box className="btn_box">
          <props.icon className="icon_box" />
          <span>{props.text}</span>
      </Box>
  );
}

export default BtnBox;
