import React from "react";
import { Card } from "@mui/material";
import dish from "../../assets/images/dish-4.png";
import HalfRating from "../Rating";
import Button from "../Button";
import BtnIcon from "../Icon-Button";
import { FaHeart, FaEye } from "react-icons/fa";
import { Grid } from "@material-ui/core";

function CardDishes(props) {
  return (
    <Card className="card_dishes">
      <div className="header_card">
        <span className="eys_icon">
          <BtnIcon icon={FaEye} />
        </span>
        <span className="heart_icon">
          <BtnIcon icon={FaHeart} />
        </span>
      </div>
      <div className="image_dishes">
        <img src={props.image} />
      </div>
      <div className="txt_card">
        <h3>Tasty Food</h3>
      </div>
      <div className="rating_dish">
        <HalfRating />
      </div>
      <div className="card_footer">
        <Grid container spacing={2} alignItems="center" justifyContent="center">
          <Grid item xs={6} sm={12} md={6}><h3>$15.99</h3></Grid>
          <Grid item xs={6}sm={12} md={6}><Button text="Add To Cart" /></Grid>
        </Grid>
        
        
      </div>
    </Card>
  );
}

export default CardDishes;
