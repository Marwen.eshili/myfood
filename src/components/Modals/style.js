import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
  title: {
    textAlign: "center",
    marginTop: "38,5px",
  },
  formInput: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    gap: "0px",
    // marginBottom: '47px',
  },
  input: {
    margin: "0px",
    padding: "0",
    marginTop: "-2px",
    //marginBottom: '5px',
  },
  inputId: {
    maxWidth: "80%",
    margin: "0px",
    marginBottom: "6px",
  },
  btnModal: {
    textAlign: "center",
    marginTop: "15px",
    marginBottom: "20px",
  },
  btn: {
    width: "112px",
    height: "32px",
    background: "#2ba7df",
    color: "white",
    border: "2px solid #2BA7DF",
    textTransform: "none",
    "&:hover, &:focus": {
      background: "white",
      color: "#2ba7df",
    },
  },
  btnFooter: {
    marginBottom: "24px",
    width: "112px",
    height: "32px",
    background: "#2ba7df",
    color: "white",
    border: "2px solid #2BA7DF",
    textTransform: "none",
    "&:hover, &:focus": {
      background: "white",
      color: "#2ba7df",
    },
  },

  boxStudent: {
    display: "flex",
    alignItems: "center",
    width: "80%",
    height: "56px",
    background: "#EBEBF3",
    borderRadius: "4px",
  },
}));

export default useStyles;
