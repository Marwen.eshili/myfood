import React from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";

const SearchModal = ({ id, open, handleClose, data, ...rest }) => {
  return (
    <Dialog
      open={open}
      onClose={() => {
        handleClose(id);
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      className="ta-modal add-video-modal"
    >
      <DialogContent>
        <h1>hello pop up</h1>
      </DialogContent>
    </Dialog>
  );
};

export default SearchModal;
