import React from "react";

function Button(props) {
  return (
    <div className="btn">
      <button
        onClick={() => {
          props.onClick();
        }}
        className="btn_b"
      >
        {props.text}
      </button>
    </div>
  );
}

export default Button;
