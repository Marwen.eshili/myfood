import * as React from "react";
import Rating from "@mui/material/Rating";
import Stack from "@mui/material/Stack";

export default function HalfRating() {
  return (
    <Stack spacing={1}>
      <Rating
        sx={{ fontSize: "30px", color: "#38b181" }}
        name="half-rating"
        defaultValue={5}
        precision={0.5}
      />
    </Stack>
  );
}
