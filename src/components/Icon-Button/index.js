import React from "react";

function BtnIcon(props) {
  return (
    <div
      className="icons"
      onClick={() => {
        props.onClick();
      }}
    >
      <props.icon className="icon_i" />
    </div>
  );
}

export default BtnIcon;
