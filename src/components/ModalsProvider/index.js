import React from "react";
import { useSelector, useDispatch } from "react-redux";

import { closeModal } from "../../Slices/modals";
import SearchModal from "../Modals/SearchModal";

const ModalsProvider = (props) => {
  const { modals } = useSelector((state) => state.modals);
  const dispatch = useDispatch();
  const modalState = (id, key) => {
    const res = modals.find((modal) => modal.id === id);
    console.log(res);
    return res[key];
  };
  const handleClose = (id) => {
    dispatch(closeModal(id));
  };
  return (
    <>
      <SearchModal
        id="search-modal"
        open={modalState("search-modal", "open")}
        data={modalState("search-modal", "data")}
        handleClose={handleClose}
      />
    </>
  );
};

export default ModalsProvider;
