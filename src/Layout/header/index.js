import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";

import Container from "@mui/material/Container";
import MenuItem from "@mui/material/MenuItem";
import Logo from "../../components/Logo";
import { makeStyles } from "@mui/styles";
import { FaHeart, FaSearch, FaShoppingCart, FaBars } from "react-icons/fa";
import BtnIcon from "../../components/Icon-Button";
import { openModal } from "../../Slices/modals";
import { useDispatch } from "react-redux";

const useStyles = makeStyles((theme) => ({}));
const pages = ["Home", "Dishes", "About", "Menu", "Review", "Order"];

const Header = () => {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  const dispatch = useDispatch();

  return (
    <AppBar
      position="fixed"
      color="inherit"
      sx={{ height: "75px", alignItems: "center", justifyContent: "center"}}
    >
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Logo />
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {pages.map((page) => (
                <MenuItem
                  key={page}
                  className="items"
                  onClick={handleCloseNavMenu}
                >
                  <h1 className="items_typo">{page}</h1>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Box
            sx={{
              flexGrow: 1,
              display: { xs: "none", md: "flex" },
            }}
            className="navbar"
          >
            <nav>
              {pages.map((page) => (
                <a href="/Dishes">{page}</a>
              ))}
            </nav>
          </Box>
          <Box sx={{ flexGrow: 0 }} className="right_icons">
            <div className="icons_res">
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="inherit"
                sx={{
                  display: { xs: "block", md: "none" },
                  color: "red",
                  backgroundColor: "#ededee",
                  borderRaduis: "50%",
                  width: "4rem",
                  height: "4rem",
                }}
              >
                <FaBars className="icon_i" />
              </IconButton>
            </div>
            <BtnIcon
              icon={FaSearch}
              onClick={() => dispatch(openModal("search-modal"))}
            />
            <BtnIcon icon={FaHeart} />
            <BtnIcon icon={FaShoppingCart} />
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default Header;
