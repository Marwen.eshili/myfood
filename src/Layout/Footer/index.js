import React from "react";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";

export default function Footer() {
  return (
    <>
      <Grid container flexDirection="column" alignItems="center">
        <Divider className="divider" />
        <div className="text_footer">
          <span className="first_txt">
            Copyright @ 2022 By
            <a
              href="https://www.linkedin.com/in/marwen-shili-66b7a51a0/"
              className="second_txt"
            >
              {" "}
              Mr. Marwen Shili
            </a>
          </span>
        </div>
      </Grid>
    </>
  );
}
