import React from "react";
import { Grid } from "@material-ui/core";

const Contact = () => {
  return (
    <div className="Contact">
      <Grid container>
        <Grid item xs={6} sm={4} md={3}>
          <div className="Contact_item">
            <h3>Locations</h3>
            <div className="links">
              <a href="#">Tunis</a>
              <a href="#">India</a>
              <a href="#">Russia</a>
              <a href="#">USA</a>
            </div>
          </div>
        </Grid>
        <Grid item xs={6} sm={4} md={3}>
          <div className="Contact_item">
            <h3>Quick Links</h3>
            <div className="links">
              <a href="#">Home</a>
              <a href="#">Dishes</a>
              <a href="#">About</a>
              <a href="#">Review</a>
              <a href="#">Order</a>
            </div>
          </div>
        </Grid>
        <Grid item xs={6} sm={4} md={3}>
          <div className="Contact_item">
            <h3>Contact Info</h3>
            <div className="links">
              <a href="#">+216 24093272</a>
              <a href="#">Food@gmail.com</a>
              <a href="#">eshilimarwen9@gmail.com</a>
              <a href="#">Sousse,Tunisie - 4200</a>
            </div>
          </div>
        </Grid>
        <Grid item xs={6} sm={4} md={3}>
          <div className="Contact_item">
            <h3>Follow Us</h3>
            <div className="links">
              <a href="#">Facebook</a>
              <a href="#">Twitter</a>
              <a href="#">Instagram</a>
              <a href="#">Linkedin</a>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default Contact;
