import React from "react";
import { Grid } from "@material-ui/core";
import CardDishes from "../../components/Card/CardDishes";
import Title from "../../components/PageTitle";
import dish1 from "../../assets/images/dish-1.png";
import dish2 from "../../assets/images/dish-2.png";
import dish3 from "../../assets/images/dish-3.png";
import dish4 from "../../assets/images/dish-4.png";
import dish5 from "../../assets/images/dish-5.png";
import dish6 from "../../assets/images/dish-6.png";

const tab = [dish1, dish2, dish3, dish4, dish5, dish6];
function Dishes() {
  return (
    <div className="section_dishes" id="Dishes">
      <div className="title">
        <Title subTitle="Our Dishes" title="POPULAR DISHES" />
      </div>
      <Grid container spacing={1} alignItems="center" justifyContent="center">
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={10}
          container
          spacing={3}
          alignItems="center"
          justifyContent="center"
        >
          {tab.map((el) => (
            <Grid
              item
              xs={12}
              sm={6}
              md={4}
              lg={4}
              container
              spacing={2}
              alignItems="center"
            >
              <CardDishes image={el} />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </div>
  );
}

export default Dishes;
