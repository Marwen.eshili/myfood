import React from "react";
import CardMenu from "../../components/CardMenu";
import Title from "../../components/PageTitle";
import { Grid } from "@material-ui/core";
import menu1 from "../../assets/images/menu-1.jpg";
import menu2 from "../../assets/images/menu-2.jpg";
import menu3 from "../../assets/images/menu-3.jpg";
import menu4 from "../../assets/images/menu-4.jpg";
import menu5 from "../../assets/images/menu-5.jpg";
import menu6 from "../../assets/images/menu-6.jpg";
import menu7 from "../../assets/images/menu-7.jpg";
import menu8 from "../../assets/images/menu-8.jpg";
import menu9 from "../../assets/images/menu-9.jpg";

const tab = [menu1, menu2, menu3, menu4, menu5, menu6, menu7, menu8, menu9];
function Menu() {
  return (
    <div className="Menu">
      <div className="title">
        <Title subTitle="Our Dishes" title="TODAY'S SPECIALITY" />
      </div>
      <Grid container spacing={1} alignItems="center" justifyContent="center">
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={10}
          container
          spacing={3}
          alignItems="center"
          justifyContent="center"
        >
          {tab.map((el) => (
            <Grid
              item
              xs={12}
              sm={6}
              md={4}
              lg={4}
              container
              spacing={2}
              lignItems="center"
            >
              <CardMenu image={el} />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </div>
  );
}

export default Menu;
