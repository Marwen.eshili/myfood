import * as React from "react";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { Formik } from "formik";
import * as Yup from "yup";

export default function PaymentForm() {
  return (
    <div className="payement_form">
    <React.Fragment>
      <Formik initialValues={{
        nameOnCard: "",
        experyDate: "",
        cardNumber: "",
        cvv: "",

      }}
        validationSchema={Yup.object().shape({
          nameOnCard: Yup.string().max(255).required("Required"),
          experyDate: Yup.date().required("Required"),
          cardNumber: Yup.number().required("Required"),
          cvv: Yup.number().required("Required"),

        })}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
          }, 400);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) => (
          <form className="form" onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <h3>Name Card</h3>
                <TextField
                  className="form_input"
                  fullWidth
                  error={Boolean(touched.nameOnCard && errors.nameOnCard)}
                  helperText={touched.nameOnCard && errors.nameOnCard}
                  inputProps={{ style: { fontSize: 15 } }}
                  InputLabelProps={{ style: { fontSize: 15 } }}
                  name="nameOnCard"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="text"
                  value={values.nameOnCard}
                  variant="outlined"
                  placeholder="name on card"
                />
              </Grid>
              <Grid item xs={12} md={6}>
              <h3>Card Number</h3>
                <TextField
                  className="form_input"
                  fullWidth
                  error={Boolean(touched.cardNumber && errors.cardNumber)}
                  helperText={touched.cardNumber && errors.cardNumber}
                  inputProps={{ style: { fontSize: 15 } }}
                  InputLabelProps={{ style: { fontSize: 15 } }}
                  name="cardNumber"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="text"
                  value={values.cardNumber}
                  variant="outlined"
                  placeholder="card number"
                />
              </Grid>
              <Grid item xs={12} md={6}>
              <h3>Expiry Date</h3>
                <TextField
                  className="form_input"
                  fullWidth
                  error={Boolean(touched.experyDate && errors.experyDate)}
                  helperText={touched.experyDate && errors.experyDate}
                  inputProps={{ style: { fontSize: 15 } }}
                  InputLabelProps={{ style: { fontSize: 15 } }}
                  name="experyDate"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="text"
                  value={values.experyDate}
                  variant="outlined"
                  placeholder="expiry date"
                />
              </Grid>
              <Grid item xs={12} md={6}>
              <h3>CVV</h3>
                <TextField
                  className="form_input"
                  fullWidth
                  error={Boolean(touched.cvv && errors.cvv)}
                  helperText={touched.cvv && errors.cvv}
                  inputProps={{ style: { fontSize: 15 } }}
                  InputLabelProps={{ style: { fontSize: 15 } }}
                  name="cvv"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="text"
                  value={values.cvv}
                  variant="outlined"
                  placeholder="cvv"
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={<Checkbox color="secondary" name="saveCard" value="yes" />}
                  label={<h2>Remember credit card details for next time</h2>}
                />
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </React.Fragment>
    </div>
  );
}
