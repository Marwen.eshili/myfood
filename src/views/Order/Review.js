import * as React from "react";
import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Grid from "@mui/material/Grid";

const products = [
  {
    name: "Product 1",
    desc: "A nice thing",
    price: "$9.99",
  },
  {
    name: "Product 2",
    desc: "Another thing",
    price: "$3.45",
  },
  {
    name: "Product 3",
    desc: "Something else",
    price: "$6.51",
  },
  {
    name: "Product 4",
    desc: "Best thing of all",
    price: "$14.11",
  },
  { name: "Shipping", desc: "", price: "Free" },
];

const addresses = ["1 MUI Drive", "Reactville", "Anytown", "99999", "USA"];
const payments = [
  { name: "Card type", detail: "Visa" },
  { name: "Card holder", detail: "Mr John Smith" },
  { name: "Card number", detail: "xxxx-xxxx-xxxx-1234" },
  { name: "Expiry date", detail: "04/2024" },
];

export default function Review() {
  return (
    <div className="review_card">
    <React.Fragment>
      <h5 variant="h6" gutterBottom>
        Order summary
      </h5>
      <List disablePadding>
        {products.map((product) => (
          <ListItem key={product.name} sx={{ py: 1, px: 0 }}>
            <ListItemText className="list_item"  variant="h1" primary={product.name} secondary={product.desc} />
            <h5 variant="body2">{product.price}</h5>
          </ListItem>
        ))}

        <ListItem sx={{ py: 1, px: 0 }}>
          <ListItemText primary="Total" />
          <h5 variant="subtitle1" sx={{ fontWeight: 700 }}>
            $34.06
          </h5>
        </ListItem>
      </List>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <h5 variant="h6" gutterBottom sx={{ mt: 2 }}>
            Shipping
          </h5>
          <h5 gutterBottom>John Smith</h5>
          <h5 gutterBottom>{addresses.join(", ")}</h5>
        </Grid>
        <Grid item container direction="column" xs={12} sm={6}>
          <h5 variant="h6" gutterBottom sx={{ mt: 2 }}>
            Payment details
          </h5>
          <Grid container>
            {payments.map((payment) => (
              <React.Fragment key={payment.name}>
                <Grid item xs={6}>
                  <h5 gutterBottom>{payment.name}</h5>
                </Grid>
                <Grid item xs={6}>
                  <h5 gutterBottom>{payment.detail}</h5>
                </Grid>
              </React.Fragment>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
    </div>
  );
}
