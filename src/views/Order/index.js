import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Toolbar from "@mui/material/Toolbar";
import Card from "@mui/material/Card";
import Button from "../../components/Button";
import { Grid } from "@material-ui/core";

import Paper from "@mui/material/Paper";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
//import Button from "@mui/material/Button";
import Link from "@mui/material/Link";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import AddressForm from "./AdressForm";
import PaymentForm from "./PayementForm";
import Review from "./Review";
import Title from "../../components/PageTitle";
import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiFormLabel-root": {
      color: "red", // or black
    },
  },
}));

const steps = ["Shipping address", "Payment details", "Review your order"];

function getStepContent(step) {
  switch (step) {
    case 0:
      return <AddressForm />;
    case 1:
      return <PaymentForm />;
    case 2:
      return <Review />;
    default:
      throw new Error("Unknown step");
  }
}

const theme = createTheme();

export default function Order() {
  const [activeStep, setActiveStep] = React.useState(0);

  const handleNext = () => {
    setActiveStep(activeStep + 1);
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };
  const classes = useStyles();

  return (
    <div className="order">
      <ThemeProvider theme={theme}>
        <div className="title_order">
          <Title title="FREE AND FAST" subTitle="Order Now" />
        </div>
        <Container component="main" maxWidth="xl" sx={{ mb: 6 }}>
          <Card
            sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 2 }, fontSize: "150%" }}
          >
            <Typography component="h1" variant="h4" align="center">
              Checkout
            </Typography>
            <Stepper activeStep={activeStep} sx={{ pt: 3, pb: 2 }}>
              {steps.map((label) => (
                <Step className="stepper" key={label}>
                  <StepLabel>
                    <h2>{label}</h2>
                  </StepLabel>
                </Step>
              ))}
            </Stepper>
            <React.Fragment>
              {activeStep === steps.length ? (
                <Grid Container alignItems="center" justifyContent="center">
                  <Grid item xs ={12} sm={6} md={6} Container alignItems="center" justifyContent="center">
                <Card sx={{backgroundColor:"#27ae60" ,padding:"10px "}}>
                  <h4  gutterBottom>
                    Thank you for your order.
                  </h4>
                  <h4 color="red">
                    Your order number is #2001539. We have emailed your order
                    confirmation, and will send you an update when your order
                    has shipped.
                  </h4>
                </Card>
                </Grid>
                </Grid>
              ) : (
                <React.Fragment>
                  {getStepContent(activeStep)}
                  <Box sx={{ display: "flex", justifyContent: "flex-end" ,gap:"10px" }}>
                    {activeStep !== 0 && (
                      <Button onClick={handleBack} text="back" >
                      </Button>
                    )}
                    <Button onClick={handleNext} text={activeStep === steps.length - 1 ? "Place order": "Next"}/>
                  </Box>
                </React.Fragment>
              )}
            </React.Fragment>
          </Card>
        </Container>
      </ThemeProvider>
    </div>
  );
}
