import React from "react";
import {
  Grid,
  Card,
  TextField,
  FormHelperText,
  Box,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Formik } from "formik";
import * as Yup from "yup";

const useStyles = makeStyles((theme) => ({}));

const Order = () => {
  const classes = useStyles();

  return (
    <Grid container justifyContent="center">
      <Grid item xs={12} sm={12} md={12}>
        <div className="card_order">
          <Formik
            initialValues={{
              name: "",
              order: "",
              number: 1,
              adress: "",
              phone: "",
              adFood: "",
              time: "",
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().max(255).required("Required"),
              order: Yup.string().max(255).required("Required"),
              number: Yup.number().required("Required"),
              adress: Yup.string().required("Required"),
              phone: Yup.number().required("Required"),
            })}
            onSubmit={(values, { setSubmitting }) => {
              setTimeout(() => {
                alert(JSON.stringify(values, null, 2));
                setSubmitting(false);
              }, 400);
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) => (
              <form className="form" onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <h3>Your Name</h3>
                    <TextField
                      className="form_input"
                      fullWidth
                      error={Boolean(touched.name && errors.name)}
                      helperText={touched.name && errors.name}
                      inputProps={{ style: { fontSize: 15 } }}
                      InputLabelProps={{ style: { fontSize: 15 } }}
                      name="name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type="text"
                      value={values.name}
                      variant="outlined"
                      placeholder="enter your name"
                    />
                    
                    <h3>Your Order</h3>
                    <TextField
                      className="form_input"
                      fullWidth
                      error={Boolean(touched.order && errors.order)}
                      helperText={touched.order && errors.order}
                      inputProps={{ style: { fontSize: 15 } }}
                      InputLabelProps={{ style: { fontSize: 15 } }}
                      name="order"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type="text"
                      value={values.order}
                      variant="outlined"
                      placeholder="enter food name"
                    />
                   
                    <h3>How Musch</h3>
                    <TextField
                      className="form_input"
                      fullWidth
                      inputProps={{ style: { fontSize: 15 } }}
                      InputLabelProps={{ style: { fontSize: 15 } }}
                      name="number"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type="number"
                      value={values.number}
                      variant="outlined"
                    />

                    <h3>Your Adress</h3>
                    <TextField
                      className="form_input"
                      fullWidth
                      error={Boolean(touched.adress && errors.adress)}
                      helperText={touched.adress && errors.adress}
                      inputProps={{ style: { fontSize: 15 } }}
                      InputLabelProps={{ style: { fontSize: 15 } }}
                      name="adress"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type="text"
                      multiline
                      rows={6}
                      maxRows={6}
                      value={values.adress}
                      variant="outlined"
                      placeholder="enter food adress"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <h3>Your Number</h3>
                    <TextField
                      className="form_input"
                      fullWidth
                      error={Boolean(touched.phone && errors.phone)}
                      helperText={touched.phone && errors.phone}
                      inputProps={{ style: { fontSize: 15 } }}
                      InputLabelProps={{ style: { fontSize: 15 } }}
                      name="phone"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type="number"
                      value={values.phone}
                      variant="outlined"
                    />
                   
                    <h3>Additional Food</h3>
                    <TextField
                      className="form_input"
                      fullWidth
                      error={Boolean(touched.adFood && errors.adFood)}
                      inputProps={{ style: { fontSize: 15 } }}
                      InputLabelProps={{ style: { fontSize: 15 } }}
                      name="adFood"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type="text"
                      value={values.adFood}
                      variant="outlined"
                      placeholder="extra with food"
                    />
                    <h3>Date and Time</h3>
                    <TextField
                      className="form_input"
                      fullWidth
                      error={Boolean(touched.adFood && errors.adFood)}
                      inputProps={{ style: { fontSize: 15 } }}
                      InputLabelProps={{ style: { fontSize: 15 } }}
                      name="time"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type="date"
                      value={values.time}
                      variant="outlined"
                      placeholder="extra with food"
                    />
                    <h3>Your Message</h3>
                    <TextField
                      className="form_input"
                      fullWidth
                      error={Boolean(touched.adFood && errors.adFood)}
                      inputProps={{ style: { fontSize: 15 } }}
                      InputLabelProps={{ style: { fontSize: 15 } }}
                      name="message"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.message}
                      type="text"
                      multiline
                      rows={6}
                      maxRows={6}
                      variant="outlined"
                      placeholder="enter your message"
                    />
                  </Grid>
                </Grid>
              </form>
            )}
          </Formik>
        </div>
      </Grid>
    </Grid>
  );
};

export default Order;
