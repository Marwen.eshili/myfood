import React from 'react';
import Title from "../../components/PageTitle";

const Reviews = () =>{
    return (
        <div className="title_review">
            <Title title="WHAT THEY SAY" subTitle="Customer's Review" />
        </div>
    )
}

export default Reviews;