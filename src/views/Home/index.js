import React from "react";
import Button from "../../components/Button";
import CardDishes from "../../components/Card/CardDishes";
import CardMenu from "../../components/CardMenu";
import Title from "../../components/PageTitle";
import pizza from "../../assets/images/home-img-2.png"
import { Grid } from "@material-ui/core";

function Home() {
  return (
    <div className="Home" >
   <Grid container spacing={2}>
     <Grid item xs={0} sm={1} md={1}></Grid>
     <Grid item xs={12} sm={10} md={10} container spacing={2}>
       <Grid item xs={12} sm={6} md={6} container alignItems="center" justifyContent="center" spacing={2}>
         <Grid item >
         <h3>Our Special Dish</h3>
         <h2>Hot Pizza</h2>
         <p>
           Lorem Ipsum Dolor Sit Amet Consectetur Adipisicing Elit. Sit Natus Dolor Cumque?
        </p>
         <Button text="Order Now" onClick={()=>{}} />
         </Grid>
        </Grid>
       <Grid item xs={12} sm={6} md={6} container alignItems="center" justifyContent="center"spacing={2}>
         <img src={pizza}></img>
        </Grid>
    </Grid>
     <Grid item xs={0} sm={1} md={1}></Grid>
   </Grid>
    </div>
  );
}

export default Home;
