import React from "react";
import Button from "../../components/Button";
import CardDishes from "../../components/Card/CardDishes";
import CardMenu from "../../components/CardMenu";
import Title from "../../components/PageTitle";
import { Grid } from "@material-ui/core";
import img from '../../assets/images/about-img.png'
import {  FaShippingFast, FaDollarSign, FaHeadset } from "react-icons/fa";
import BtnBox from '../../components/BtnBox'
function About() {
  return (
    <div className="About" >
    <div className="title">
      <Title subTitle="About Us" title="WHY CHOOSE US?" />
    </div>
    <Grid container spacing={3}>
      <Grid item xs={1} sm={1} md={1}></Grid>
      <Grid item xs={10} sm={10} md={10} container >
          <Grid item xs={12} sm={12} md={6} container alignItems="center" justifyContent="center">
              <img src={img}></img>
          </Grid>
          <Grid item xs={12} sm={12} md={6}>
              <h3>Best Food In The Country</h3>
              <p>
                Lorem Ipsum Dolor Sit Amet Consectetur Adipisicing Elit. Dolore, Sequi Corrupti Corporis Quaerat Voluptatem Ipsam Neque Labore Modi Autem, Saepe Numquam Quod Reprehenderit Rem? Tempora Aut Soluta Odio Corporis Nihil! <br />
                  Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elit. Aperiam, Nemo. Sit Porro Illo Eos Cumque Deleniti Iste Alias, Eum Natus.
              </p>
              <Grid container spacing={1}>
              <Grid item xs={6} sm={6} md={6}>
                <BtnBox text="Free Delivery" icon={FaShippingFast}/>
              </Grid>
              <Grid item xs={6} sm={6} md={6}>
              <BtnBox text="Free Delivery" icon={FaDollarSign}/>
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
              <BtnBox text="Free Delivery" icon={FaHeadset}/>
              </Grid>
              <span className="btn_footer_grid_right">
              <Button  text="Learn more" onClick={()=>{}} />
              </span>
              </Grid>
          </Grid>
      </Grid>
      <Grid item xs={1} sm={1} md={1} ></Grid>

    </Grid>
    </div>
  );
}

export default About;